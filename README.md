# Waffle-tile-generator

Some OpenCV code that makes a waffle tile image

Having watched CNC Kitchen's nice [yt video](https://youtu.be/3-ygdNQThAs) about texturing 3d prints for strength, I wanted to try, but couldn't find a good waffle pattern tile on the internet.
So I made a simple generator, and here it is.
