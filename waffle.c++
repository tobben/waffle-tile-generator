#include <cmath>
#include <cstddef>
#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>   // waitKey
#include <opencv2/imgcodecs.hpp> // IMREAD_COLOR/IMREAD_UNCHANGED/IMREAD_GREYSCALE

double gaussian(int row, int mu) {
  auto const variance{14500};
  auto const bottomWidth{1.01};
  return bottomWidth * 255 *
         std::exp(-0.5 * (row - mu) * (row - mu) / variance);
};

auto main(int const argc, char **const argv) -> int {
  auto constexpr rows{3000};
  auto constexpr cols{3000};
  cv::Scalar const background{0};

  cv::Mat img{rows, cols, CV_8UC1, background};
  for (auto col{0}; col < cols; ++col) {
    for (auto row{0}; row < rows; ++row) {
      img.at<std::byte>(row, col) = static_cast<std::byte>(
          std::min(gaussian(row, col) + gaussian(row, cols - col), 255.0));
    }
  }

  cv::namedWindow("Display image", cv::WINDOW_NORMAL);
  cv::resizeWindow("Display image", rows, cols);
  cv::imshow("Press s to save image. Any other key to continue", img);
  // Wait for a keystroke in the window
  // Save with s
  // Quit with any other keystroke
  std::cout << "Press s to save image. Any other key to continue" << std::endl;
  if (cv::waitKey(0) == 's') {
    cv::imwrite("waffle.png", img);
  }

  cv::bitwise_not(img, img);
  cv::imshow("Press s to save inverted image. Any other key to exit", img);
  if (cv::waitKey(0) == 's') {
    cv::imwrite("inverted-waffle.png", img);
  }

  return 0;
}
